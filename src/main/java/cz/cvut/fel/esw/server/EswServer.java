package cz.cvut.fel.esw.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Tsimafei Raro
 */
public class EswServer implements Runnable {

    protected ConcurrentHashMap<String, Integer> words;
    private final int port;
    private ServerSocket server;
    private final ExecutorService threadPool;

    public EswServer(int port, int threadCount) {
        this.port = port;
        this.threadPool = Executors.newFixedThreadPool(threadCount);
    }

    private void startServer() {
        try {
            words = new ConcurrentHashMap<>();
            server = new ServerSocket(port);
        } catch (IOException e) {
            System.out.println("IOE on server startup");
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        startServer();
        while (true) {
            try {
                Socket clientSocket = server.accept();
                threadPool.execute(new RequestHandler(clientSocket, words));
            } catch (IOException e) {
                e.printStackTrace();
                threadPool.shutdown();
                System.out.println("Server has been stopped");
            }
        }
    }
}
