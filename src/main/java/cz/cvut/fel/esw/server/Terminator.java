package cz.cvut.fel.esw.server;

/**
 * @author Tsimafei Raro
 */
public class Terminator implements Runnable {
    private final long timeToLiveMillis;

    public Terminator(long timeToLiveMillis) {
        this.timeToLiveMillis = timeToLiveMillis;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(timeToLiveMillis);
            System.out.println("TERMINATOR: Time to live has elapsed, killing the server");
            System.exit(0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
