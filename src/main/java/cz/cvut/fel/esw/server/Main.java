package cz.cvut.fel.esw.server;

/**
 * @author Tsimafei Raro
 */
public class Main {

    private final static int DEFAULT_PORT = 8080;
    private final static int DEFAULT_TTL = 120_000;
    private final static int DEFAULT_THREAD_COUNT = 24;

    public static void main(String[] args) {
        int port = (args.length > 0)
                ? Integer.parseInt(args[0])
                : DEFAULT_PORT;
        int timeToLive = (args.length > 1)
                ? Integer.parseInt(args[1])
                : DEFAULT_TTL;
        int threadCount = (args.length > 2)
                ? Integer.parseInt(args[2])
                : DEFAULT_THREAD_COUNT;

        System.out.println("running on port " + port);
        System.out.println("will be killed after " + timeToLive + "ms");
        System.out.println("using " + threadCount + " threads");

        new Thread(new EswServer(port, threadCount)).start();
        new Thread(new Terminator(timeToLive)).start();
    }
}
