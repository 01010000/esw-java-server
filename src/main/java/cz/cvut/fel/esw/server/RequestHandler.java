package cz.cvut.fel.esw.server;

import cz.cvut.fel.esw.server.proto.Request;
import cz.cvut.fel.esw.server.proto.Response;

import java.io.*;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.zip.GZIPInputStream;

/**
 * @author Tsimafei Raro
 */
public class RequestHandler implements Runnable {
    private final Socket socket;
    private final ConcurrentHashMap<String, Integer> words;

    public RequestHandler(Socket socket,
                          ConcurrentHashMap<String, Integer> words) {
        this.socket = socket;
        this.words = words;
    }

    @Override
    public void run() {
        try {
            DataInputStream input = new DataInputStream(socket.getInputStream());
            OutputStream out = socket.getOutputStream();
            while (true) {
                Request request;
                try {
                    request = readRequestFromInput(input);
                } catch (EOFException eof) {
                    break;
                }
                Response response = handleRequest(request);
                sendResponse(response, out);
            }
            socket.shutdownInput();
            socket.shutdownOutput();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Request readRequestFromInput(DataInputStream input) throws IOException {
        int requestSize = input.readInt();
        byte[] requestRaw = new byte[requestSize];
        input.readFully(requestRaw);
        return Request.parseFrom(requestRaw);
    }

    private Response handleRequest(Request request) throws IOException {
        Response response;
        if (request.hasGetCount()) {
            int count = handleGetCountRequest();
            response = Response.newBuilder()
                    .setStatus(Response.Status.OK)
                    .setCounter(count)
                    .build();
        } else if (request.hasPostWords()) {
            handlePostWordsRequest(request);
            response = Response.newBuilder()
                    .setStatus(Response.Status.OK)
                    .build();
        } else {
            response = Response.newBuilder()
                    .setStatus(Response.Status.ERROR)
                    .setErrMsg("Unknown message")
                    .build();
        }
        return response;
    }

    private int handleGetCountRequest() {
            int size = words.size();
            words.clear();
            return size;
    }

    private void handlePostWordsRequest(Request request) throws IOException {
        InputStream gzipped = request.getPostWords().getData().newInput();
        InputStreamReader reader = new InputStreamReader(new GZIPInputStream(gzipped), StandardCharsets.UTF_8);
        BufferedReader bufferedReader = new BufferedReader(reader);
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            StringTokenizer tokenizer = new StringTokenizer(line);
            while (tokenizer.hasMoreTokens()) {
                words.put(tokenizer.nextToken(), 1);
            }
        }
    }

    private void sendResponse(Response response,
                              OutputStream output) throws IOException {
        int responseLength = response.getSerializedSize();
        ByteBuffer buffer = ByteBuffer.allocate(responseLength + 4);
        buffer.putInt(responseLength);
        buffer.put(response.toByteArray());
        output.write(buffer.array());
    }
}
