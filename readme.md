# ESW 2021 - Java server
## How to run
1. Build the jar via `mvn package`.
2. Start the application via `java -jar target/java-server-1.0-jar-with-dependencies.jar
`. 

Requires Java 8 to run. Other than that, the jar is entirely self-dependent, as it packages all dependencies.

The application takes three optional parameters:
1. Port on which to run - defaults to 8080
2. Time-to-live in ms - after how long should the application terminate (since the server has an infinte loop, I wanted a nice little safeguard so it wouldn't clog up resources for too long) - defaults to 120 000ms.
3. The amount of threads which should be used - defaults to 24. 